package com.example.android.uamp.service;

import com.example.android.uamp.entity.TargetEntity;

public class TargetEntityService {

    private static TargetEntityService instance;

    private TargetEntity targetEntity;
    // here we can inject other serbices for property extracting

    private TargetEntityService() {}

    public static synchronized TargetEntityService getInstance() {
        if (instance == null) {
            instance = new TargetEntityService();
        }
        return instance;
    }

    // implement methods for property extracting
    // it's preferred to use injected services

    public void serialize() {
        // implement entity saving to file
    }

    public void clear() {
        this.targetEntity = new TargetEntity();
    }

}
