package com.example.android.uamp.entity;

public class TargetEntity {

    private PersonInfo      personInfo;
    private TrackInfo       trackInfo;
    private PlaybackInfo    playbackInfo;

    public PersonInfo getPersonInfo() {
        return personInfo;
    }

    public void setPersonInfo(PersonInfo personInfo) {
        this.personInfo = personInfo;
    }

    public TrackInfo getTrackInfo() {
        return trackInfo;
    }

    public void setTrackInfo(TrackInfo trackInfo) {
        this.trackInfo = trackInfo;
    }

    public PlaybackInfo getPlaybackInfo() {
        return playbackInfo;
    }

    public void setPlaybackInfo(PlaybackInfo playbackInfo) {
        this.playbackInfo = playbackInfo;
    }
}
