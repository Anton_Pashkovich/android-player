package com.example.android.uamp.entity;

import java.util.Date;
import java.util.List;

public class PlaybackInfo {

    private Date                        startDate;
    private Date                        endDate;
    private List<ArousalValenceData>    avList;
    // add fields for seeking data

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public List<ArousalValenceData> getAvList() {
        return avList;
    }

    public void setAvList(List<ArousalValenceData> avList) {
        this.avList = avList;
    }
}
