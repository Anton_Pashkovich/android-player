package com.example.android.uamp.entity;

public class ArousalValenceData {

    private Double      x;
    private Double      y;
    private Integer     time;   // time (ms)

    public Double getX() {
        return x;
    }

    public void setX(Double x) {
        this.x = x;
    }

    public Double getY() {
        return y;
    }

    public void setY(Double y) {
        this.y = y;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }
}
